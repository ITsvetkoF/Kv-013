(function () {
    'use strict';

    angular.module('app', [
        'app.core',
        'app.widgets',
        'app.dashboard',
        'app.layout',
        'app.repos',
        'app.roles',
        'ui.bootstrap',
        'app.login'
    ]);
})();
