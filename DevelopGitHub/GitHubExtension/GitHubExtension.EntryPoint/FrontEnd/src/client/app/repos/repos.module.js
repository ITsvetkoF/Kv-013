﻿(function () {
    'use strict';

    angular.module('app.repos', [
        'app.core',
        'app.widgets'
    ]);
})();
