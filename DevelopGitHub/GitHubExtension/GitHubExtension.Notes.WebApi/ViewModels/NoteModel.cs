namespace GitHubExtension.Notes.WebApi.ViewModels
{
    public class NoteModel
    {
        public string UserId { get; set; }
        public string CollaboratorId { get; set; }
        public string Body { get; set; }
    }
}