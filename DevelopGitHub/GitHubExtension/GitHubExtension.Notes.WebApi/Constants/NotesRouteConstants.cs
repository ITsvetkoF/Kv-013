﻿namespace GitHubExtension.Notes.WebApi.Constants
{
    public class NotesRouteConstants
    {
        public const string GetNoteRoute = "api/note/collaborators/{collaboratorId}";
        public const string CreateNoteRoute = "api/note";
    }
}